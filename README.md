# Stationery, a Hugo Theme

**Stationery** is a theme for the lovely [Hugo static site generator](https://gohugo.io) inspired by the clean lines and beautiful type of print stationery, updated and streamlined for the reality of 21st-century web publishing.

Particular care and effort has gone into the typography, including a custom full-featured webfont build of Raleway which is featured prominently as a header and display typeface.

![Several devices showing blogs using variants of the Stationery theme](screenshots/theme-preview-variants.png)

## Features

Stationery is targeted at a hybrid personal/professional site, built from the ground up with reusability and stylistic customization in mind. Whether you just want a standalone landing page or a full-featured blog site, Stationery has you covered.

Some stand-out features include:

- Responsive, mobile-first layout&mdash;as all things should be.
- Typography-focused design, including exacting vertical rhythm and a comfortable maximum line length.
- Fully customizable colors, which are automatically value-graded and programmatically expanded into a full Material Design-style color palette.
- Dynamically generated monogram logo in the theme primary color; just specify your initials.
  - Alternatively, Gravatar support is included to easily sync up with the rest of your social media/GitHub presence.
  - You can also of course manually specify a logo image by URL.
- Extremely lightweight and quick to load, even with all the high-quality included webfonts. No expensive and bulky JQuery here, just static, minified, modern CSS.
  - Seriously, the heaviest thing about this theme is rendering the SVG monogram logo.

## Demo

My personal website at benjaminlodi.com serves as the *de facto* demo for the theme. A proper demo page is coming Soon&trade;.

## Requirements

Stationery is designed and built with the extended edition of Hugo, leveraging [Hugo Pipes](https://gohugo.io/hugo-pipes/introduction) to compile and minify the styles. This allows all sorts of neat magic, like reading custom colors from the Hugo `config.toml` and generating a full color palette from there.

Unfortunately doesn't support local installs of postcss-cli on all platforms, so to be sure you'll need to have `postcss-cli` installed globally and available in your path.

```bash
npm install -g postcss-cli
# or
yarn global add postcss-cli
```

That said, Stationery with its default color scheme should work out of the box with the standard version of Hugo. You'll still be able to customize the initials in the logo, but tweaking colors and other aspects of the theme will be difficult or impossible. This is actually how I run it on my own site, which is simply built and deployed using the default GitLab Pages template for Hugo.

## Installation

Simply clone this repository into your `<site>/themes` directory, or alternatively add it as a submodule. Then update your Hugo `config.toml` or `config.yaml` with

```toml
# TOML
theme = hugo-theme-stationery
```

```yaml
# YAML
theme: hugo-theme-stationery
```

If you want to customize the theme colors or typography, make sure you have Hugo Extended installed and `postcss` is available in your path. Otherwise Hugo will fall back to the pre-built files and you'll get the lovely default mint green no matter what you do.

## Configuration

Stationery uses your site title and other information as specified in the standard Hugo site config file. It also supports extra configurable options under the `params` key.

```yaml
# Example configuration (YAML)
params:
  dateFormat: 2 Jan 2006
  mainLogoGravatar: false
  monogram: BRL
  colors:
    primary: '#669966'
    accent: theme-cyan
    red: salmon
    dark: rgb(20, 37, 18)
```

### `monogram` (string, default `STR`)

Set the text for the monogram logo. There's no enforced restrictions here, but two- and three-letter capitalized initials typically work best.

### `mainLogoGravatar` (boolean, default: `false`)

If `true`, the theme will use the Gravatar associated with the primary author's email address instead of the default monogram logo.

### `logoImg` (URL, optional)

If provided, the theme will use the image at the given URL instead of the monogram logo or a Gravatar.

### `dateFormat` (string, default `2006-01-02`)

Specify a format for "fancy" date display throughout the theme. This uses the [Go time.Format string specification](https://golang.org/pkg/time/#Time.Format), which works by specifying how you'd expect the "magical reference date" of `Mon Jan 2 15:04:05 MST 2006` to be written.

### `disableCredits` (boolean, default: `false`)

If `true`, the theme will not output the "Built with Hugo..." text in the footer.

### `colors` (map)

Override the colors used by the theme. You can specify any of the following values as the new color:

- **hex color** - must be quoted, e.g. `'#336699'`
- **rgb(a) color** - e.g. `rgb(255, 170, 200)` or `rgba(100, 0, 100, 0.5)`
- **named CSS color** - e.g. `antiquewhite` or `rebeccapurple`

Additionally, the non-color keys (marked with &dagger; below) also accept the following special values to refer back to the defined colors in the theme palette (including any changes you've applied to them in this section)

- `theme-red`
- `theme-orange`
- `theme-yellow`
- `theme-green`
- `theme-cyan`
- `theme-blue`
- `theme-magenta`

Note that there's some magic going on, so the resulting color values on a given element in your Web Inspector may not always exactly match the values passed in (e.g. links use a darker shade of the primary color).

| keys        | purpose (often an automatically-generated variant is used)   |
| ----------- | ------------------------------------------------------------ |
| `primary`&dagger;   | logo color, links, and other bold elements           |
| `accent`&dagger;    | secondary emphasis, like the border of block quotes  |
| `dark`&dagger;      | primary font color                                   |
| `light`&dagger;     | main body background, logo text color                |
| `logo`&dagger;      | logo color specifically (takes precedence over `primary`) |
| `logo-text`&dagger; | logo text color specifically (takes precedence over `light`) |
|             |                                                              |
| `red`       | override default red shade used in the theme palette        |
| `orange`    | override default orange shade used in the theme palette     |
| `yellow`    | override default yellow shade used in the theme palette     |
| `green`     | override default green shade used in the theme palette      |
| `cyan`      | override default cyan shade used in the theme palette       |
| `blue`      | override default blue shade used in the theme palette       |
| `magenta`   | override default magenta shade used in the theme palette    |

#### Example colors config

```yaml
params:
  colors:
    blue: cornflowerblue
    primary: theme-blue # this will be cornflowerblue
    accent: theme-orange
    logo-text: '#006'
```

## Contributing

I welcome feedback issues, feature requests, and reasonable pull requests. This is my first full foray into Hugo theme development, after some experience with Jekyll, so I'm sure there are things that could be done better or more idiomatically.
