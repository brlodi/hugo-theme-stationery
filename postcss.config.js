module.exports = {
  plugins: [
    require('postcss-gap-properties'),
    require('autoprefixer'),
    require('postcss-normalize'),
    require('postcss-inline-svg')({
      paths: ['themes/hugo-theme-stationery/static'],
    }),
    require('cssnano')({
      preset: 'advanced',
    }),
  ],
};
